/**
 * Grunt configuration
 */
module.exports = function(grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    /*******************************************************************************************************************
     * Define core grunt config parameters
     ******************************************************************************************************************/
    var _config;

	_config = {
        application: {
            applicationPath: 'app'
        }
    };

    /*******************************************************************************************************************
     * Work with server, tasks and watchers
     ******************************************************************************************************************/
    _config.watch = {
        'compass': {
            files: [
                '<%= application.applicationPath %>/ui/styles/scss/{,*/}*.{scss,sass}',
                '<%= application.applicationPath %>/ui/styles/scss/**/{,**/}*.{scss,sass}'
            ],
            tasks: ['compass']
        },
        'livereload': {
            options: {
                livereload: '<%= connect.options.livereload %>'
            },
            files: [
                '<%= application.applicationPath %>/{,*/}*.html',
                '<%= application.applicationPath %>/ui/styles/css/{,*/}*.css',
                '<%= application.applicationPath %>/ui/scripts/{,**/}**.js',
                '<%= application.applicationPath %>/ui/fonts/{,**/}**',
                '<%= application.applicationPath %>/ui/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
            ]
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Set compass options /////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _config.compass = {
        options: {
            // cssDir: '.tmp/styles',
            relativeAssets: false,
            debugInfo: false,
        },
        'css': {
            options: {
                sassDir: '<%= application.applicationPath %>/ui/styles/scss',
                cssDir: [
                    '<%= application.applicationPath %>/ui/styles/css'
                ]
            }
        },
        'images': {
            options: {
                httpImagesPath: 'ui/images',
                imagesDir: '<%= application.applicationPath %>/ui/images',
            }
        },
        'fonts': {
            options: {
                httpFontsPath: 'ui/fonts',
                fontsDir: '<%= application.applicationPath %>/ui/fonts'
            }
        },
        'javascript': {
            options: {
                javascriptsDir: '<%= application.applicationPath %>/ui/scripts'
            }
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Set connection options //////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var port, livereloadport; // Port from 3000 to 9000 with step 100
	port = grunt.option('port') && grunt.option('port') >= 3000 && grunt.option('port') <= 9000
        ? parseInt(grunt.option('port'), 10)
        : 7000; 
        //: (3000 + Math.ceil(Math.Rand * 10) * (9 - 3) * 1000);
	livereloadport = grunt.option('livereloadport')
            && (grunt.option('livereloadport') === 1337 || grunt.option('livereloadport') === 35729)
        ? parseInt(grunt.option('livereloadport'), 10)
        : 35729;
    _config.connect = {
        options: {
            port: port,
            // Change this to '0.0.0.0' to access the server from outside.
            hostname: '127.0.0.1',
            livereload: livereloadport
        },
        livereload: {
            options: {
                open: true,
                base: [
                    '<%= application.applicationPath %>'
                ]
            }
        },
    };

    /*******************************************************************************************************************
     * INIT CONFIG
     ******************************************************************************************************************/
    grunt.initConfig(_config);

    /*******************************************************************************************************************
     * Define default grunt commands
     ******************************************************************************************************************/
    /**
     * Get grunt commands info
     *
     * @example <caption>Get grunt info:</caption>
     * grunt
     * grunt default
     *
     * @syntax grunt [default]
     */
    grunt.registerTask('default', function() {
        console.info(_infoMessage());
    });

    /**
     * Run server from developer machine. Server will run from core folder (not dist). All watchers will be on
     *
     * Don't build application in a distinct folder
     *
     * @syntax grunt run
     *
     * @param {array[string]} arguments
     */
    grunt.registerTask('run', 'Run server from core folders (developer mode)', function() {
        // Task started
        grunt.log.writeln('Task started : Run server from core folders (developer mode)');

        // Run the task
        grunt.task.run([
            'connect:livereload',
            'watch'
        ]);
    });

    /*******************************************************************************************************************
     * Helpers
     ******************************************************************************************************************/
    /**
     * Output of the information about current grunt commands

     * @return {string} Formatted information text
     */
    function _infoMessage() {
        return '\n--------------------------------------------------\n'
            + 'Avaliable commands are : \n'
            + '\n----------\n'
            + '* RUN SERVER WITH WATCHERS:\n'
            + '  Format:\n'
            + '    grunt run [--port=PORT_TO_CONNECT] [--livreloadport=LIVERELOAD_PORT]\n'
            + '  Parameters:\n'
            + '  - PORT_TO_CONNECT - port for connection from range [3000; 9000], if not set 7000 will be used\n'
            + '  - LIVERELOAD_PORT - possible livereload ports: 1337 and 35729, use 35729 on default\n'
            + '  Usage:\n'
            + '    grunt run\n'
            + '    grunt run --port=3000\n'
            + '\n--------------------------------------------------\n'
            +
            '\nStrongly not recommended to use other grunt commands, defined as sub-tasks or defined in the Config,\n'
            + '\if you are not sure that you know what to do.\n'
            + '\nEnjoy Grunt tasks with our team :)\n';
    }
};