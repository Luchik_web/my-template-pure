var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var _enviroment = 'dev';
//var browserSync = require('browser-sync').create();
//var connect = require('gulp-connect-php');


var cssLessPath = './static/assets/less';
var cssPath = './static';

gulp.task('default', function() {
    console.log(
        'Use commands : '
        + '\n Compile css files: \n'
        + '    gulp compile'
        + '\n Watch html/css: \n'
        + '    gulp watch'
        );
});

gulp.task('compile', function() {
    return _lessHelper('style');
});

function _lessHelper(fname) {
    return gulp.src(cssLessPath + '/' + fname + '.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest(cssPath))

        .pipe(minifyCSS())
        .pipe(replace('http://', '//'))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(cssPath));
}

/**
 * Watch css/html
 */
gulp.task('watch', [], function() {
    // ---------- PROCESS CSS --------------------------------------------------
    gulp.watch([
        cssLessPath + '/theme/**.less',
        cssLessPath + '/landing/**.less',
        cssLessPath + '/style.less'
    ], ['compile']);
});